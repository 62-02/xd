package com.example.examenrecuperacion;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ImcActivity extends AppCompatActivity {

    private EditText editHeight, editWeight;
    private TextView txtImcResult, txtWelcome;
    private Button btnCalculateImc, btnClear, btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        editHeight = findViewById(R.id.editHeight);
        editWeight = findViewById(R.id.editWeight);
        txtImcResult = findViewById(R.id.txtImcResult);
        txtWelcome = findViewById(R.id.txtWelcome);
        btnCalculateImc = findViewById(R.id.btnCalculateImc);
        btnClear = findViewById(R.id.btnClear);
        btnBack = findViewById(R.id.btnBack);

        String name = getIntent().getStringExtra("name");
        txtWelcome.setText("Bienvenido " + name);

        btnCalculateImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateImc();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editHeight.setText("");
                editWeight.setText("");
                txtImcResult.setText("IMC: ");
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calculateImc() {
        String heightStr = editHeight.getText().toString();
        String weightStr = editWeight.getText().toString();

        if (TextUtils.isEmpty(heightStr) || TextUtils.isEmpty(weightStr)) {
            Toast.makeText(this, "Por favor, rellena todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        float height = Float.parseFloat(heightStr);
        float weight = Float.parseFloat(weightStr);

        float imc = weight / (height * height);
        txtImcResult.setText(String.format("IMC: %.2f", imc));
    }
}
